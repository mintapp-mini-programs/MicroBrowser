import 'package:flutter/material.dart';
import 'package:wordpress_app/services/app_service.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:share/share.dart';

class BrowserMiniApp extends StatefulWidget {
  final String? title;
  final String? url;
  const BrowserMiniApp({Key? key, required this.url, required this.title}) : super(key: key);

  @override
  _BrowserMiniAppState createState() => _BrowserMiniAppState();
}

Future _handleShare(String url) async {
  Share.share(url);
}
Future _handleReport() async {
  AppService().openEmailSupport();
}

class _BrowserMiniAppState extends State<BrowserMiniApp> {
  late WebViewController theWebViewController;
  TextEditingController theController = new TextEditingController();
  bool showLoading = false;

  void updateLoading(bool ls) {
    this.setState(() {
      showLoading = ls;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text(widget.title!),
        centerTitle: true,
        actions: [
          Container(
            padding: EdgeInsets.only(right: 15),
            child: PopupMenuButton(
              /*shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(15)),
              ),*/
                enableFeedback: true,
                elevation: 1,
                padding: EdgeInsets.all(5),
                child: Icon(
                  CupertinoIcons.ellipsis,
                  size: 20,

                ),
                itemBuilder: (BuildContext context) {
                  return <PopupMenuItem>[
                    PopupMenuItem(
                      value: 'share',
                      child: Row(
                        children: <Widget>[
                          Icon(Feather.share),
                          SizedBox(
                            width: 10,
                          ),
                          Text("Share"),
                        ],
                      ),
                    ),
                    PopupMenuItem(
                      value: 'report',
                      child: Row(
                        children: <Widget>[
                          Icon(Feather.flag),
                          SizedBox(
                            width: 10,
                          ),
                          Text("Report"),
                        ],
                      ),
                    ),
                  ];
                },
                onSelected: (dynamic value) {
                  if (value =='share') {
                    _handleShare(widget.url!);
                  } else if (value =='report') {
                    _handleReport();
                  } else {
                    _handleShare(widget.url!);
                  }
                }),
          )
        ],

      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            /*Flexible(
              flex: 1,
              child: Container(
                color: Theme.of(context).primaryColor,
                child: Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Expanded(
                          flex: 1,
                          child: Text(
                            "https://",
                            style: TextStyle(color: Colors.white, fontSize: 20),
                          )),
                      Expanded(
                        flex: 3,
                        child: TextField(
                          autocorrect: false,
                          style: TextStyle(color: Colors.white, fontSize: 20),
                          decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(
                                    style: BorderStyle.solid,
                                    color: Colors.white,
                                    width: 2),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(
                                    style: BorderStyle.solid,
                                    color: Colors.white,
                                    width: 2),
                              )),
                          controller: theController,
                        ),
                      ),
                      Flexible(
                        flex: 1,
                        child: Center(
                          child: IconButton(
                              icon: Icon(
                                Icons.search,
                                color: Colors.white,
                              ),
                              onPressed: () {
                                String finalURL = theController.text;
                                if (!finalURL.startsWith("https://")) {
                                  finalURL = "https://" + finalURL;
                                } else {
                                  finalURL = finalURL;
                                }
                                if (true) {
                                  updateLoading(true);
                                  theWebViewController
                                      .loadUrl(finalURL)
                                      .then((onValue) {})
                                      .catchError((e) {
                                    updateLoading(false);
                                  });
                                }
                              }),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),*/
            Expanded(
                flex: 12,
                child: Stack(
                  children: <Widget>[
                    WebView(
                      initialUrl: widget.url,
                      onPageFinished: (data) {
                        updateLoading(false);
                      },
                      javascriptMode: JavascriptMode.unrestricted,
                      onWebViewCreated: (webViewController) {
                        theWebViewController = webViewController;
                      },
                    ),
                    (showLoading)
                        ? Center(
                      child: CircularProgressIndicator(),
                    )
                        : Center()
                  ],
                )),
          ],
        ),
      ),
    );
  }
}
